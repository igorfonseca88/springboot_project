package com.dsp.springboot.rest.tournament;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

import java.util.List;
import java.util.Optional;

import javax.xml.ws.WebServiceException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.dsp.springboot.rest.team.Team;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TournamentResourceTest {

	@Mock
	private TournamentRepository tournamentRepository;

	@InjectMocks
	private TournamentResource tournamentResource;

	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@Test
	public void createTournament() {

		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

		List<Team> teams = asList(
                new Team(1L, "Boston Celtics", 123, 456),
                new Team(2L, "Toronto Raptors", 123, 456)
        );
		Tournament tournament = new Tournament();
		tournament.setId(1L);
		tournament.setName("Basketball A");
		tournament.setTeams(teams);

		when(tournamentRepository.save(tournament)).thenReturn(tournament);

		ResponseEntity<Object> savedTeamResponse = tournamentResource.createTournament(tournament);

		verify(tournamentRepository, times(1)).save(tournament);
		assertEquals(HttpStatus.CREATED, savedTeamResponse.getStatusCode());
		assertTrue(savedTeamResponse.getHeaders().getFirst("Location").endsWith(Long.toString(tournament.getId())));

	}

	@Test
	public void listTournaments() {

		Tournament tournamentFirstWeek = new Tournament();
		tournamentFirstWeek.setId(1L);
		tournamentFirstWeek.setName("Basketball A");

		List<Tournament> allTournaments = asList(tournamentFirstWeek);

		when(tournamentRepository.findAll()).thenReturn(allTournaments);
		List<Tournament> results = tournamentResource.listTournaments();
		assertEquals(results.size(), 1);

	}

	@Test
	public void getTournament() {
		Tournament tournament = new Tournament();
		tournament.setId(1L);
		tournament.setName("Basketball A");

		when(tournamentRepository.findById(1L)).thenReturn(Optional.of(tournament));

		assertEquals(tournamentResource.getTournament(1L), new ResponseEntity<>(tournament, HttpStatus.OK));

		ResponseEntity<Tournament> notFoundTeamResponse = tournamentResource.getTournament(2);

		assertEquals(HttpStatus.NOT_FOUND, notFoundTeamResponse.getStatusCode());
	}
	
	@Test
	public void generateFixtures() {
		Tournament tournament = new Tournament();
		tournament.setId(1L);
		tournament.setName("Basketball A");
		
        when(tournamentRepository.findById(1L)).thenReturn(Optional.of(tournament));

        exceptionRule.expect(WebServiceException.class);
        exceptionRule.expectMessage("No Such Tournament");
        tournamentResource.generateFixtures(2);

	}
}
