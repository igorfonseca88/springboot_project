package com.dsp.springboot.rest.fixture;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


import com.dsp.springboot.rest.team.Team;
import com.dsp.springboot.rest.tournament.Tournament;


@RunWith(MockitoJUnitRunner.class)
public class FixtureResourceTest {

	@Mock
	private FixtureRepository fixtureRepository;

	@InjectMocks
	private FixtureResource fixtureResource;
	
	@Test
	public void getFixturesByWeekTournament() {
		Team homeTeam = new Team(1L, "Boston Celtics", 123, 456);
		Team awayTeam = new Team(2L, "Toronto Raptors", 123, 456);
        
		Tournament tournament = new Tournament();
		tournament.setId(1L);
		tournament.setName("Basketball A");
		
		List<Fixture> fixtures = asList(new Fixture(tournament, homeTeam, awayTeam, 1 ));

		when(fixtureRepository.getFixturesByWeekTournament(1L)).thenReturn(Optional.of(fixtures));
		
		List<Fixture> results = fixtureResource.getFixturesByWeekTournament(1L);
        assertEquals(results.size(), 1);

		results = fixtureResource.getFixturesByWeekTournament(3L);

        assertEquals(results, Collections.emptyList());
	}
}
