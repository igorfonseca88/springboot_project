package com.dsp.springboot.rest.fixture;

import com.dsp.springboot.rest.fixture.Fixture;
import com.dsp.springboot.rest.fixture.FixtureRepository;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;


@RestController
public class FixtureResource {


    @Autowired
    private FixtureRepository fixtureRepository;
    
    
    @GetMapping("/fixtures/{week}/week")
    public List<Fixture> getFixturesByWeekTournament(@PathVariable long week) {
        return fixtureRepository.getFixturesByWeekTournament(week)
            .orElse(Collections.emptyList());
    }
    
    
}
