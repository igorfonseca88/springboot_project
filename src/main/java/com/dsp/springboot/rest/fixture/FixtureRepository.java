package com.dsp.springboot.rest.fixture;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface FixtureRepository extends JpaRepository<Fixture, Long> {
    // TODO: Implement correct query here
    @Query(value = "SELECT * FROM fixture where HOME_TEAM_ID = ?1 or AWAY_TEAM_ID = ?1", nativeQuery = true)
    Optional<List<Fixture>> getFixturesByTeamId(long teamId);
    
    @Query(value = "SELECT * FROM fixture where week = ?1", nativeQuery = true)
    Optional<List<Fixture>> getFixturesByWeekTournament(long week);
}
