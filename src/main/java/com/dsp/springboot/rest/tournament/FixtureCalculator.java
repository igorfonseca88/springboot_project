package com.dsp.springboot.rest.tournament;

import com.dsp.springboot.rest.fixture.Fixture;
import com.dsp.springboot.rest.team.Team;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FixtureCalculator {

	private Tournament tournament;
	static final Long TEAM_ID_ODD = -1L;
	static final int NUMBER_OF_ROUNDS = 2;

	public FixtureCalculator(Tournament tournament) {
		this.tournament = tournament;
	}

	public List<Fixture> calculateFixtures() {
		// TODO: Implement schedule creation here

		List<Fixture> fixtures = new ArrayList<>();
		Fixture fixture = null;
		ArrayList<Team> teamsList = new ArrayList<Team>(tournament.getTeams());
		int week = 1;

		Collections.sort(teamsList, (o1, o2) -> o1.getId().compareTo(o2.getId()));

		if (tournament.getTeams().size() % 2 == 1) {
			Team teamOdd = new Team();
			teamOdd.setId(TEAM_ID_ODD);
			teamsList.add(teamOdd);
		}

		int totMatches = (teamsList.size() - 1) * NUMBER_OF_ROUNDS;

		for (int round = 0; round < totMatches; round++) {
			for (int match = 0; match < (teamsList.size() / 2); match++) {

				Team teamHome = new Team();
				Team teamAway = new Team();
				teamHome = teamsList.get((round + match) % (teamsList.size() - 1));
				teamAway = teamsList.get((teamsList.size() - 1 - match + round) % (teamsList.size() - 1));

			
				if (match == 0) {
					teamAway = teamsList.get(teamsList.size() - 1);
				}


				if (round < (teamsList.size() - 1)) {
					fixture = new Fixture(tournament, teamHome, teamAway, week);
				} else {
					fixture = new Fixture(tournament, teamAway, teamHome, week);
				}

				// removing last team - if odd remove odd team else if even
				// remove last team
				if (!teamAway.equals(teamsList.get(teamsList.size() - 1))) {
					fixtures.add(fixture);
				}

			}
			week++;
		}

		return fixtures;
	}

}
