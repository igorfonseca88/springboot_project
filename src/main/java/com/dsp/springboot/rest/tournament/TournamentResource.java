package com.dsp.springboot.rest.tournament;

import com.dsp.springboot.rest.fixture.Fixture;
import com.dsp.springboot.rest.fixture.FixtureRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.xml.ws.WebServiceException;

@RestController
public class TournamentResource {

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private FixtureRepository fixtureRepository;

    //TODO: Create new tournament POST endpoint
    
    @PostMapping("/tournaments")
    public ResponseEntity<Object> createTournament(@RequestBody Tournament tournament) {
    	Tournament tournamentObj = tournamentRepository.save(tournament);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(tournamentObj.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping("/tournaments")
    public List<Tournament> listTournaments() {
        return tournamentRepository.findAll();
    }

    @GetMapping("/tournaments/{id}")
    public ResponseEntity<Tournament> getTournament(@PathVariable long id) {
        return tournamentRepository.findById(id)
            .map(tournament -> new ResponseEntity<>(tournament, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/tournaments/{id}/generateFixtures")
    public List<Fixture> generateFixtures(@PathVariable long id) {
        Optional<Tournament> tournament = tournamentRepository.findById(id);
        if (!tournament.isPresent())
            throw new WebServiceException("No Such Tournament");
        FixtureCalculator fixtureCalculator = new FixtureCalculator(tournament.get());
        List<Fixture> fixtures = fixtureCalculator.calculateFixtures();
        fixtureRepository.saveAll(fixtures);
        return fixtures;
    }
}
