/* TODO: Put your js code here */


function retrieveTeams(query) {
	
	const baseUrl = "http://localhost:8080/teams?name=";
	let endPoint = (query == "" || query == undefined) ? baseUrl : baseUrl + query;
	
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', endPoint);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
            	console.log(xhr.responseText);
                const resp = JSON.parse(xhr.responseText);
                
                if (resp.error) {
                    reject(resp.error);
                } else {
                    resolve(createTeamsTable(resp));
                }
            }
        }
        xhr.send();
    })
}

function createTeamsTable(teams){
	
	let table = "";
	let rows = "";
	const startTable = "<table><thead><tr><th>Name</th><th>Year Founded</th><th>Stadium Capacity</th></tr></thead><tbody>";
	const endTable = "</tbody></table>";

	console.log(teams);
	for(i=0;i<teams.length;i++) {
		rows += "<tr>" +
        "<td>" + teams[i].name + "</td>" +
        "<td>" + teams[i].yearFounded + "</td>" +
        "<td>" + teams[i].stadiumCapacity + "</td>" +
        "</tr>";
	}
	
	table = startTable + rows + endTable;
	document.getElementById('teamQueryResult').innerHTML = table;
}

window.onload = function () {
	retrieveTeams();
	document.getElementById('teamQuery').addEventListener("keyup", event => {
	  if (event.isComposing || event.keyCode === 229) {
	    return;
	  }
	  retrieveTeams(event.target.value);
	});
}

